package com.shapravskyi.menu;

import com.shapravskyi.utils.Fibonacci;

import java.util.Scanner;

/**
 * <h1>Menu Class</h1>
 *  Creates a simple console menu and,
 *  depending on the user's choice,
 *  takes some action or closes the program.
 *
 *  @author Andrii Shapravskyi
 *  @version 1.0
 *  @since 2018-11-11
 */

public class Menu {
    private Scanner scanner;

    public Menu() {
        scanner = new Scanner(System.in);
    }

    /**
     * The method proposes to enter a number corresponding to the menu item
     * and based on the number closes the program
     * or calls a method for outputting the Fibonacci sequence.
     */
    public void menuInitialization() {
        printMenu();
        int chooseNumber = Integer.parseInt(scanner.nextLine());

        switch (chooseNumber) {
            case 0:
                System.exit(0);
                break;
            case 1:
                System.out.print("Enter interval: ");
                int interval = Integer.parseInt(scanner.nextLine());
                Fibonacci.printSequence(interval);
                break;
            default:
                System.out.println("Choose correct number.");
                break;
        }
    }

    /**
     * This method displays the menu items and
     * instructions for the user on the console
     */

    private void printMenu() {
        System.out.println("Choose one of the following:\n");
        System.out.println("\t1. Calculate fibonacci numbers");
        System.out.println("\t0. Exit\n");
        System.out.print("Enter the number: ");
    }
}
