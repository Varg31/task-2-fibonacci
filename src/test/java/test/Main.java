package test;


import com.shapravskyi.menu.Menu;

public class Main {
    /**
     * This is the main method in which the Menu object is created
     * @param args Unused.
     */
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.menuInitialization();
    }
}